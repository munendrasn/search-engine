# Search Engine

A simple search engine in Python for keyword based search.
This doesn't make use any Database.

## Page Ranking Algorithm

Ranking of Pages is based on number of inlinks and outlinks.
Number of inlinks has positive impact on ranking whereas outlinks has negative impact.

## Usage

1. Run the Program
```
python search_engine.py
```
2. Provide seed page and maximum number of pages to crawl

3. Provide Key to be serached
