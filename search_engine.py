"""
simple search engine based on keyword search
"""
import requests


def get_all_links(page):
    """
    finds all links present in a given page.
    """
    links = []
    start_link = page.find('<a href=')  # finding links
    while start_link != -1:
        start_quote = page.find('"', start_link)
        end_quote = page.find('"', start_quote+1)
        url = page[start_quote+1 : end_quote]
        links = [url] + links
        page = page[end_quote:]
        start_link = page.find('<a href=')

    return links


def add_to_index(index, keyword, url):
    """
    builds bucket for given key.
    """
    if keyword in index:
        index[keyword].append(url)
    else:
        index[keyword] = [url]


def add_page_to_index(index, url, content):
    """
    creates buckets with keyword as key and urls as its values.
    """
    words_list = content.split()
    for word in words_list:
        add_to_index(index, word, url)


def crawl_web(seed, max_pages):
    """
    web crawler.
    """
    tocrawl = [seed]
    crawled = []
    index = {}
    graph = {}
    while tocrawl:
        url = tocrawl.pop()
        if url not in crawled and len(crawled) < max_pages:
            content = requests.get(url).text
            add_page_to_index(index, url, content)
            outlinks = get_all_links(content)
            graph[url] = outlinks
            for alink in outlinks:
                if alink not in tocrawl:
                    tocrawl = [alink] + tocrawl
            crawled.append(url)

    return index, graph


def lookup(index, keyword):
    """
    searches for the keyword and returns the urls containing that keyword.
    """
    if keyword in index:
        return index[keyword]
    else:
        return None


def compute_ranks(graph):
    """
    generates ranks for the pages.
    """
    d = 0.8
    numloops = 10

    ranks = {}
    npages = len(graph)

    for page in graph:
        ranks[page] = 1.0/npages

    for i in range(0, numloops):
        newranks = {}
        for page in graph:
            newrank = (1.0 - d) / npages
            for node in graph:
                if page in graph[node]:
                    newrank += d * ranks[page]/len(graph[node])
            newranks[page] = newrank
        ranks = newranks

    return ranks


def output_links(search_key, index, ranks):
    """
    outputs links in sorted order for given search key.
    """
    links = lookup(index, search_key)
    if links is None:
        print "No Web Page"
        return
    res = []
    for i in index[search_key]:
        if i in ranks:
            ele = [i, ranks[i]]
            res.append(ele)

    sorted_list = sorted(res)
    for result in sorted_list:
        print result[0]


if __name__ == "__main__":

    seed = raw_input('Give the seed page : ')
    max_pages = int(raw_input('Give the maximum number of pages to crawl : '))
    index, graph = crawl_web(seed, max_pages)
    ranks = compute_ranks(graph)
    search_key = raw_input('Give the search key : ')
    output_links(search_key, index, ranks)
